	var qasicoApp = angular.module('qasicoApp', ['ngRoute']);

	qasicoApp.config(function($routeProvider) {
		$routeProvider

			.when('/', {
				templateUrl : 'pages/home.html',
				controller  : 'mainController'
			})

			.when('/signup', {
				templateUrl : 'pages/signup.html',
				controller  : 'signupController'
			})
	});

	qasicoApp.controller('mainController', function($scope, $location) {
		$scope.background = './lib/img/bg_home.jpg';
		$scope.isActive = function (viewLocation) {
		     var active = (viewLocation === $location.path());
		     return active;
		};
	});

	qasicoApp.controller('signupController', function($scope) {
		$scope.background = './lib/img/bg_signup.jpg';
	});